---
title:
- Boite à outil Libre pour les réalités étendues spatiale
author:
- Nicolas Bouillot
institute:
- Société des arts technologiques
theme:
- moon
date:
- 18 octobre 2022
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

# 

![](img/slide1.jpg){width=120%}

#

![](./img/slide2.jpg){width=120%}
